package types

import (
	"encoding/json"
	"time"
)

type APIDuration struct {
	*time.Duration
}

func NewAPIDuration(duration *time.Duration) *APIDuration {
	if duration == nil {
		return nil
	}
	return &APIDuration{Duration: duration}
}
func (d *APIDuration) MarshalJSON() ([]byte, error) {
	return json.Marshal(int(d.Seconds()))
}
