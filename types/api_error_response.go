package types

type APIErrorsResponse struct {
	Errors []APIErrorResponse `json:"errors"`
}

type APIErrorResponse struct {
	Status      int          `json:"status"`
	IsPermanent bool         `json:"is_permanent"`
	RetryIn     *APIDuration `json:"retry_in,omitempty"`
}
