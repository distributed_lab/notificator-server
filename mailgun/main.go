package mailgun

import (
	"encoding/base64"

	notificator "gitlab.com/distributed_lab/notificator-server/client"

	"github.com/pkg/errors"

	"github.com/mailgun/mailgun-go"
	"gitlab.com/distributed_lab/notificator-server/conf"
)

// SendEmail send email throw Mailgun service.
func SendEmail(destination, subject, message string, file *notificator.File, mailgunConf conf.MailgunConf) (string, string, error) {
	mg := mailgun.NewMailgun(mailgunConf.Domain, mailgunConf.Key)
	if mailgunConf.ApiBase != "" {
		mg.SetAPIBase(mailgunConf.ApiBase)
	}
	msg := mg.NewMessage(mailgunConf.From, subject, "", destination)
	msg.SetHtml(message)

	if file != nil {
		attachment, err := base64.StdEncoding.DecodeString(file.File)
		if err != nil {
			return "", "", errors.Wrap(err, "failed to decode file")
		}
		msg.AddBufferAttachment(file.FileName, attachment)
	}

	return mg.Send(msg)
}
