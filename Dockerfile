FROM golang:1.9

WORKDIR /go/src/gitlab.com/distributed_lab/notificator-server
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -o /usr/local/bin/notificator -v gitlab.com/distributed_lab/notificator-server/cmd
RUN chmod +x /usr/local/bin/notificator

FROM ubuntu:latest
COPY --from=0 /usr/local/bin/notificator /usr/local/bin/notificator
COPY --from=0 /go/src/gitlab.com/distributed_lab/notificator-server/migrations /migrations
RUN apt-get update && apt-get install -y ca-certificates

EXPOSE 9009

