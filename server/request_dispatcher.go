package server

import (
	"time"

	"gitlab.com/distributed_lab/notificator-server/conf"
	"gitlab.com/distributed_lab/notificator-server/limiter"
	"gitlab.com/distributed_lab/notificator-server/types"
)

type DispatchResultReasonType int

const (
	DispatchResultSuccess DispatchResultReasonType = iota
	DispatchResultUnknownType
	DispatchResultLimitExceeded
)

type RequestDispatcher struct {
}

type DispatchResult struct {
	Type        DispatchResultReasonType
	IsPermanent bool
	RetryIn     *time.Duration
}

func NewRequestDispatcher() *RequestDispatcher {
	return &RequestDispatcher{}
}

func (d *RequestDispatcher) Dispatch(request *types.APIRequest, requestType conf.RequestType) (*DispatchResult, error) {
	var checkResults []*limiter.CheckResult
	for _, limiter := range requestType.Limiters {
		checkResult, err := limiter.Check(request)
		if err != nil {
			return nil, err
		}

		checkResults = append(checkResults, checkResult)
	}

	dispatchResult := &DispatchResult{Type: DispatchResultSuccess}
	for _, result := range checkResults {
		if result.Success {
			continue
		}

		dispatchResult.Type = DispatchResultLimitExceeded

		if result.IsPermanent {
			dispatchResult.IsPermanent = true
			break
		}

		if dispatchResult.RetryIn == nil || *dispatchResult.RetryIn < *result.RetryIn {
			dispatchResult.RetryIn = result.RetryIn
		}
	}

	return dispatchResult, nil
}
