package server

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/sirupsen/logrus"

	"gitlab.com/distributed_lab/notificator-server/conf"
	"gitlab.com/distributed_lab/notificator-server/q"
	"gitlab.com/distributed_lab/notificator-server/types"
)

type APIService struct {
	router            *chi.Mux
	requestDispatcher *RequestDispatcher
	requests          conf.RequestsConf
	log               *logrus.Entry
}

func NewAPIService(log *logrus.Logger, requests conf.RequestsConf) *APIService {
	return &APIService{
		router:            chi.NewRouter(),
		requestDispatcher: NewRequestDispatcher(),
		requests:          requests,
		log:               log.WithField("service", "api"),
	}
}

func (service *APIService) Init(cfg conf.Config) {
	r := service.router

	// middleware
	r.Use(
		Recover(service.log),
		ContentTypeMiddleware("application/json"),
		LogMiddleware(service.log),
		CheckAuthMiddleware(cfg.HTTP().AllowUntrusted),
	)
	// routes
	r.Post("/", service.rootHandler)
	r.Post("/batch", service.Batch)
}

func (service *APIService) Run(cfg conf.Config) {
	addr := fmt.Sprintf("%s:%d", cfg.HTTP().Host, cfg.HTTP().Port)

	service.log.WithField("starting on ", addr).Info()
	err := http.ListenAndServe(addr, service.router)
	if err != nil {
		service.log.WithError(err).Fatal("terminated")
	}
}

func (service *APIService) rootHandler(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, `{"reason": "internal server error", "msg": "%s"}`, err)
		return
	}

	apiRequest := new(types.APIRequest)
	err = json.Unmarshal(body, apiRequest)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, `{"reason": "invalid request body", "msg": "%s"}`, err)
		return
	}

	var result *DispatchResult

	requestType, ok := service.requests.Get(apiRequest.Type)
	if !ok {
		result = &DispatchResult{Type: DispatchResultUnknownType}
	}

	result, err = service.requestDispatcher.Dispatch(apiRequest, requestType)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, `{"reason": "internal server error", "msg": "%s"}`, err)
		return
	}

	code, msg := renderResponse(*result)
	if code == http.StatusOK {
		err := q.Request().Insert(*types.NewRequest(
			requestType.ID,
			requestType.Priority,
			apiRequest.PayloadString.Raw(),
			apiRequest.Token,
			apiRequest.GetHash(),
		))
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintf(w, `{"reason": "internal server error", "msg": "%s"}`, err)
			return
		}
	}

	w.WriteHeader(code)
	w.Write([]byte(msg))
}

func (service *APIService) Batch(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, `{"reason": "internal server error", "msg": "%s"}`, err)
		return
	}

	var apiRequests []types.APIRequest
	err = json.Unmarshal(body, &apiRequests)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, `{"reason": "invalid request body", "msg": "%s"}`, err)
		return
	}

	var (
		dispatchResults []DispatchResult
		requestTypes    []types.Request
	)
	for _, request := range apiRequests {
		var result *DispatchResult
		requestType, ok := service.requests.Get(request.Type)
		if !ok {
			result = &DispatchResult{Type: DispatchResultUnknownType}
		}

		result, err = service.requestDispatcher.Dispatch(&request, requestType)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintf(w, `{"reason": "internal server error", "msg": "%s"}`, err)
			return
		}

		requestTypes = append(requestTypes, *types.NewRequest(
			requestType.ID,
			requestType.Priority,
			request.PayloadString.Raw(),
			request.Token,
			request.GetHash()))
		dispatchResults = append(dispatchResults, *result)
	}

	code, msg := renderResponse(dispatchResults...)
	if code == http.StatusOK {
		err := q.Request().Insert(requestTypes...)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintf(w, `{"reason": "internal server error", "msg": "%s"}`, err)
			return
		}
	}

	w.WriteHeader(code)
	w.Write([]byte(msg))
}

//renderResponse check all dispatch result and
//if anyone of dispatch result will have no success
//return code and message for specified status code
func renderResponse(results ...DispatchResult) (responseCode int, msg string) {
	for _, result := range results {
		if result.Type == DispatchResultSuccess {
			responseCode = http.StatusOK
			msg = `{}`
			continue
		}

		if result.Type == DispatchResultLimitExceeded {
			responseCode = http.StatusTooManyRequests
			response := types.APIErrorsResponse{
				Errors: []types.APIErrorResponse{
					{
						Status:      http.StatusTooManyRequests,
						IsPermanent: result.IsPermanent,
						RetryIn:     types.NewAPIDuration(result.RetryIn),
					},
				},
			}
			body, err := json.Marshal(&response)
			if err != nil {
				panic(err)
			}

			return responseCode, string(body)
		}

		return http.StatusBadRequest, `{}`
	}

	return responseCode, msg
}
