package server

import (
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/distributed_lab/logan/v3/errors"
)

func Recover(log *logrus.Entry) func(h http.Handler) http.Handler {
	return func(h http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			defer func() {
				if rec := recover(); rec != nil {
					err := errors.FromPanic(rec)
					log.WithError(err).Error("service panicked")
				}
			}()
			h.ServeHTTP(w, r)
		}
		return http.HandlerFunc(fn)
	}
}
